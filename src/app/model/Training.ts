export class Training {
    trainingID?: number;
    topic?: string;
    start_time?: string;
    end_time?: string;
    training_date?: string;
    trainer?: any;
    trainerID?: string;
}