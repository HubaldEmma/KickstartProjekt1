export class Trainer {
    trainer_id?: number;
    first_name?: string;
    last_name?: string;
    email?: string;
    job_title?: string;
    telephone?: number;
    linkedin_link?: string;
    profile_pic?: string;
    topics?: string;
}