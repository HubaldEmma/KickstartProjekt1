import { TestBed } from '@angular/core/testing';

import { PostTopicsService } from './post-topics.service';


describe('PostTopicsService', () => {
  let service: PostTopicsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PostTopicsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
