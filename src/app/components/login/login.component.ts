import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  auth?: any;
  username?: string;
  password?: string;

  constructor(private http: HttpClient,private authService: AuthService , private router: Router) { }

  ngOnInit(): void {
    if(localStorage.getItem("admin") || localStorage.getItem("student")){
      this.router.navigate(['/home']).then(() => {
        window.location.reload();
        });
    }

  }
  clickSubmit(){
    this.getAuth();

  }

  getAuth() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    const pw = { "username": this.username, "password": this.password };
    //Admin = MyAdmin2021
    // = MyStart2021
    //this.http.options<any>('https://44xocdu788.execute-api.eu-central-1.amazonaws.com/default/logIn-py);
    this.http.post<any>('http://ec2-3-70-12-106.eu-central-1.compute.amazonaws.com:8100/api/pwIn/logIn',pw  ).subscribe(data => {
        this.auth = data.groupname;
        if(this.auth == "admin"){
          localStorage.setItem("admin", "1");
          this.router.navigate(['/home']).then(() => {
            window.location.reload();
            });
        }else if(this.auth == "general"){
          localStorage.setItem("student", "1");
          this.router.navigate(['/home']).then(() => {
            window.location.reload();
            });
        }else {
          alert("Falsche Daten!");
        }

    });
  }

}
