import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  student?: string | null;
  admin?: string | null;
  constructor(private router: Router) { }

  ngOnInit(): void {
    this.student = localStorage.getItem("student") ? localStorage.getItem("student") : "0";
    this.admin = localStorage.getItem("admin") ? localStorage.getItem("admin") : "0";
    if(this.admin == "0" && this.student == "0"){ 
     this.router.navigate([""]);
     console.log("test");
    }
  }

}
