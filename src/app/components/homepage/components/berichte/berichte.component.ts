import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-berichte',
  templateUrl: './berichte.component.html',
  styleUrls: ['./berichte.component.css']
})
export class BerichteComponent implements OnInit {

  @Input () name?: string;
  @Input () time?: string;
  @Input () text?: string;
  @Input () pb2?: string;


  constructor() { }

  ngOnInit(): void {
  }

}
