import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IntromovieComponent } from './intromovie.component';

describe('IntromovieComponent', () => {
  let component: IntromovieComponent;
  let fixture: ComponentFixture<IntromovieComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IntromovieComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IntromovieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
