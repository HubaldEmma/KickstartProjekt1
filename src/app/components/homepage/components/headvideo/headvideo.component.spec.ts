import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeadvideoComponent } from './headvideo.component';

describe('HeadvideoComponent', () => {
  let component: HeadvideoComponent;
  let fixture: ComponentFixture<HeadvideoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeadvideoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadvideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
