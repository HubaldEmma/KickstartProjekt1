import { Component, Input, OnInit } from '@angular/core';
import { Path } from 'typescript';

@Component({
  selector: 'app-important-contact',
  templateUrl: './important-contact.component.html',
  styleUrls: ['./important-contact.component.css']
})
export class ImportantContactComponent implements OnInit {

  @Input () name?: string;
  @Input () job?: string;
  @Input () mail?: string;
  @Input () phone?: string;
  @Input () pb?: string;




  constructor() { }

  ngOnInit(): void {
  }

}

