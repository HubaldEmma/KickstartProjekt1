import { Component, OnInit } from '@angular/core';
import { NgbProgressbarConfig } from '@ng-bootstrap/ng-bootstrap';
import { Topics } from 'src/app/model/Topics';

import { PostTopicsService } from 'src/app/services/progressbar/post-topics.service';

@Component({
  selector: 'app-progressbar',
  templateUrl: './progressbar.component.html',
  styleUrls: ['./progressbar.component.css'],
  providers: [NgbProgressbarConfig],
})
export class ProgressbarComponent implements OnInit {
  _value?: any = this.getLocalStorage("progressbarValue") != null ? this.getLocalStorage("progressbarValue") : 0;
  topics?: Topics[];
  _topics: string[] = [];
  _numTopics: number = 0;


  constructor(config: NgbProgressbarConfig, private postTopicsService : PostTopicsService) { 
    config.max = this._numTopics;
    config.striped = false;
    config.animated = false;
    config.type = 'primary';
    config.height = '15px';
  }

  ngOnInit(): void {
    //this._value = this.getLocalStorage("progressbarValue") != null ? this.getLocalStorage("progressbarValue") : 0;
     
    this.getTopics();

    //Platzhalter-Werte für korrekte Anzeige der Progressbar
    //this._topics = ["Start","Java","Business","HTML", "CSS","DB", "Python", "Bias","Ende"];
    //this._numTopics = this._topics.length-1.1;

    //console.log("HALLO",this.topics);
    //console.log(this._topics)
    

  }

  fillProgress(fill: number){
    this._value =  fill;
    this.upadateValue();
  }

  upadateValue() {
    localStorage.setItem("progressbarValue", this._value.toString());
  }

  getLocalStorage(name: string){
    return localStorage.getItem(name);
  }

  getTopics(): void {
    this.postTopicsService.list()
      .subscribe(
        data => {
          this.topics = data;
          //console.log(this.topics);
          this._numTopics = this.topics ? this.topics.length  - 0.9 : 9;
        },
        error => {
          console.error(error);
        });
  }

}
