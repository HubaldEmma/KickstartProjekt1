import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit{
  admin: string | null = localStorage.getItem("admin") ? localStorage.getItem("admin"): "0";
  student: string | null = localStorage.getItem("student") ? localStorage.getItem("student"): "0";
  navbarOpen = false;


  constructor(private router: Router) {
  }
  
  ngOnInit() {
    this.admin = localStorage.getItem("admin") ? localStorage.getItem("admin"): "0";
    this.student= localStorage.getItem("student") ? localStorage.getItem("student"): "0";
  }
  logout(){
    localStorage.clear();
    this.router.navigate(['']).then(() => {
      window.location.reload();
      });
  }

  toggleNavbar() {

    this.navbarOpen = !this.navbarOpen;

  }



}
