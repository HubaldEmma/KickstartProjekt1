import { Component, OnInit } from '@angular/core';
import { Trainer } from 'src/app/model/Trainer';

import { PostTrainerService } from 'src/app/services/trainer/post-trainer.service';

import { ActivatedRoute, Router } from '@angular/router';
import { NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-admin-trainer',
  templateUrl: './admin-trainer.component.html',
  styleUrls: ['./admin-trainer.component.css']
})
export class AdminTrainerComponent implements OnInit {
  trainer?: Trainer[];
  first_name: string = '';
  last_name?: string;
  job_title?: string;
  email?: string;
  telephone?: number;
  linkedInUrl?: string;
  topics?: string;
  trainer_info?: string;
  isEdit?: boolean = false;
  buttonTitle: string = "Speichern";


  trainerObj: Trainer = {
    trainer_id: 0,
    first_name: "",
    last_name: "",
    job_title: "",
    email: "",
    telephone: 0,
    linkedin_link: "",
    profile_pic: "",
    topics: ""
  };

  trainerBeforeEdit: Trainer = {
    trainer_id: 0,
    first_name: "",
    last_name: "",
    job_title: "",
    email: "",
    telephone: 0,
    linkedin_link: "",
    profile_pic: "",
    topics: ""
  };


  constructor(private postTrainerService : PostTrainerService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.getTrainer();
  }

  submitButton(){
    this.trainerObj.first_name=this.first_name;
    this.trainerObj.last_name=this.last_name;
    this.trainerObj.job_title=this.job_title;
    this.trainerObj.email=this.email;
    this.trainerObj.telephone=this.telephone;
    this.trainerObj.linkedin_link=this.linkedInUrl;
    this.trainerObj.topics=this.topics;
    //this.trainerObj.trainer_info=this.trainer_info;

    if(!this.isEdit){

      if (this.first_name && this.last_name){
        this.createNewTrainer();
      }
      else {
        alert("Es wurden nicht alle Felder ausgefüllt!");
      }
    }
    else {
      console.log(this.trainerObj.trainer_id, this.trainerObj)
      if(this.first_name && this.last_name){
        this.saveEditedTrainer(this.trainerObj.trainer_id ? this.trainerObj.trainer_id.toString() : "", this.trainerObj);
        this.isEdit = false;
        this.buttonTitle = "Speichern";
        this.trainerObj.trainer_id = 0;
      }else {
        alert("Es wurden nicht alle Felder ausgefüllt!");
      }
    }

    this.first_name=""; this.last_name=""; this.job_title=""; this.email=""; this.telephone=undefined; this.linkedInUrl=""; this.topics=""; this.trainer_info="";
  }


  editButton(trainer_id?: number, trainerObj?: Trainer) {

    this.first_name = trainerObj?.first_name ? trainerObj?.first_name : "";
    this.last_name = trainerObj?.last_name;
    this.job_title = trainerObj?.job_title;
    this.email = trainerObj?.email;
    this.telephone = trainerObj?.telephone;
    this.linkedInUrl = trainerObj?.linkedin_link ;
    this.topics = trainerObj?.topics;
    //this.trainer_info = trainerObj.trainer_info;

    this.trainerObj.trainer_id = trainerObj?.trainer_id;

    console.log("Edit", trainerObj)
    this.isEdit = true;
    this.buttonTitle = "Änderungen speichern";
  }

  deleteButton(trainerId?: number){
    this.deleteTrainer(trainerId ? trainerId.toString() : " ");
  }

  deleteTrainer(id: string): void {
    this.postTrainerService.delete(id)
      .subscribe(
        response => {
          this.refreshList();
        },
        error => {
          console.error(error);
        });
       console.log(id)
  }

  getTrainer(): void {
    this.postTrainerService.list()
      .subscribe(
        data => {
          this.trainer = data;
          console.log(this.trainer);
        },
        error => {
          console.error(error);
        });
  }

  private saveEditedTrainer(trainerID?: string, trainerObj?: Trainer) {
    this.postTrainerService.update(trainerID ? trainerID : "", trainerObj ? trainerObj : {})
      .subscribe(
        response => {
          //this.router.navigate([ '/admin' ]);
          //console.log(trainerID); console.log(trainerID)
          this.refreshList();
        },
        error => {
          console.error(error);
        });
  }

  private createNewTrainer() {
    this.postTrainerService.create(this.trainerObj)
      .subscribe(
        response => {
          this.router.navigate([ '/admin' ]);
          this.refreshList();
        },
        error => {
          console.error(error);
        });
  }
  

  refreshList(): void {
    this.getTrainer();
  }

}
