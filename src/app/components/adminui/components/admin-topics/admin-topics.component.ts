import { Component, OnInit } from '@angular/core';
import { PostTopicsService } from 'src/app/services/progressbar/post-topics.service';
import { Topics } from 'src/app/model/Topics';
import { isThisSecond } from 'date-fns';

@Component({
  selector: 'app-admin-topics',
  templateUrl: './admin-topics.component.html',
  styleUrls: ['./admin-topics.component.css']
})
export class AdminTopicsComponent implements OnInit {
  topics?: Topics[];
  
  topicsObj: Topics = {
    topicId: 0,
    topicInfo: "",
    topicsid: 0,
    topicsinfo: "",

  };
  topicsObjList: any;
  topicList?: any[9] = [{
    topic1: "",
    topic2: "", 
    topic3: "",
    topic4: "",
    topic5: "",
    topic6: "",
    topic7: "",
    topic8: "",
    topic9: "",
  }];
  topicListBeforeEdit: any[9] = [{
    topic1: "",
    topic2: "", 
    topic3: "",
    topic4: "",
    topic5: "",
    topic6: "",
    topic7: "",
    topic8: "",
    topic9: "",
  }];

  

  constructor(private postTopicsService: PostTopicsService) { }

  ngOnInit(): void {
    this.getTopics();


  }

  saveButton() {
    if (this.topicList){
      for(let i = 0; i < this.topicList.length; i++){
        if(this.topics && this.topicListBeforeEdit[i] == this.topics[i].topicInfo){
          this.topicsObj.topicsid = this.topics[i].topicId;
          this.topicsObj.topicsinfo = this.topicList[i];

          if (this.topicsObj.topicsid){
            //console.log(this.topicsObj.topicsid.toString(), this.topicsObj)
            this.saveEditedPost(this.topicsObj.topicsid.toString(), this.topicsObj)
          }
        }

      }
    }
  }


  getTopics(): void {
    this.postTopicsService.list()
      .subscribe(
        data => {
          this.topics = data;
          console.log(this.topics);
          if (this.topicList && this.topics){
            for(let i = 0; i < this.topics?.length; i++){
              this.topicList[i] = this.topics[i].topicInfo;
              this.topicListBeforeEdit[i] = this.topics[i].topicInfo;
            }
          }
        },
        error => {
          console.error(error);
        });
  }


  private saveEditedPost(topicID?: string, topicObj?: Topics) {
    this.postTopicsService.update(topicID ? topicID : "", topicObj ? topicObj : {})
      .subscribe(
        response => {
          //this.router.navigate([ '/admin' ]);
          console.log(topicID); console.log(topicObj)
          this.refreshList();
        },
        error => {
          console.error(error);
        });
  }

  refreshList(): void {
    this.getTopics();
  }

}
