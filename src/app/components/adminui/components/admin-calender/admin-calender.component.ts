import { Component, OnInit } from '@angular/core';
import { Training } from 'src/app/model/Training';
import { PostTrainingService } from 'src/app/services/training/post-training.service';
import { PostTrainerService } from 'src/app/services/trainer/post-trainer.service';
import { Trainer } from 'src/app/model/Trainer';

import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-admin-calender',
  templateUrl: './admin-calender.component.html',
  styleUrls: ['./admin-calender.component.css']
})
export class AdminCalenderComponent implements OnInit {
  training?: Training[];
  trainer?: Trainer[];
  eventObj?: any;
  trainerName?: string;
  isEdit?: boolean = false;
  buttonTitle: string = "Speichern";

  //input
  date?: string;
  start_time?: string;
  end_time?: string;
  trainer_input?: any;
  topic?: string;

  trainingObj: Training = {
    trainingID: 0,
    topic: "",
    start_time: "",
    end_time: "",
    training_date: "",
    trainer: "",
    trainerID:"",
  }

  trainingObjBeforeEdit: Training = {
    trainingID: 0,
    topic: "",
    start_time: "",
    end_time: "",
    training_date: "",
    trainer: "",
    trainerID: "",
  }


  trainerForm = new FormGroup({
    website: new FormControl('', Validators.required)
  });
  
  get f(){
    return this.trainerForm.controls;
  }

  submit(){
    console.log(this.trainerForm.value)
    if (this.trainer != null){
      for (let curTrainer of this.trainer){
        if(this.trainerForm.value.website === (curTrainer.first_name + " " + curTrainer.last_name)){
          this.trainingObj.trainerID = curTrainer.trainer_id?.toString();
        }
      }
    }
    
  }
  changeWebsite(e: any) {
    //console.log(e.target.value);
  }


  constructor(private postTrainingService: PostTrainingService , private postTrainerService: PostTrainerService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.getPosts();
    this.getTrainer();
  }

  
  async submitButton() {

    this.trainingObj.topic=this.topic;
    this.trainingObj.start_time=this.start_time;
    this.trainingObj.end_time=this.end_time;
    this.trainingObj.training_date=this.date;

    /*
    if (this.trainer != null){
      for (let curTrainer of this.trainer){
        if(this.trainerForm.value.website === (curTrainer.first_name + " " + curTrainer.last_name)){
          this.trainingObj.trainerID = curTrainer.trainer_id?.toString();
        }
      }
    }
    */
    // ersetzt:
    this.trainingObj.trainerID = this.trainerForm.value.website;
      

    if (this.training != null){
      for (let curTraining of this.training){
        if(this.trainingObjBeforeEdit.topic == curTraining.topic && this.trainingObjBeforeEdit.training_date == curTraining.training_date && this.trainingObjBeforeEdit.start_time == curTraining.start_time && this.trainingObjBeforeEdit.end_time == curTraining.end_time){
          this.trainingObj.trainingID = curTraining.trainingID;
        }
      }
    }


    if(!this.isEdit){
      if(this.topic && this.start_time && this.date && this.trainerForm.value.website != 0){
        this.createCalendarEvent();
      }else {
        alert("Es wurden nicht alle Felder ausgefüllt!");
      }
    }
    else {
      //console.log(this.trainerForm.value.website)
      console.log(this.trainerForm.value.website)
      if(this.topic && this.start_time && this.date && this.trainerForm.value != 0 && this.trainerForm.value.website != "" && this.trainingObj.trainerID != ""){
        this.saveEditedPost(this.trainingObj.trainingID ? this.trainingObj.trainingID.toString() : "", this.trainingObj);
        this.isEdit = false;
        this.buttonTitle = "Speichern";
        console.log(this.trainingObj)
      }else {
        console.log(this.trainerForm.value)
        alert("Es wurden nicht alle Felder ausgefüllt!");
      }
    }
    this.topic = ""; this.start_time = ""; this.end_time = ""; this.date = ""; this.trainer_input = ""; this.trainerForm.setValue({website: 0})
    

}

  deleteButton(trainingId?: number) {
    console.log(trainingId);
    this.deleteEvent(trainingId ? trainingId.toString() : "");
  }

  editButton(trainingId?: number, trainingObj?: Training) {
    //console.log(trainingId);
    //console.log(trainingObj);

    this.trainingObjBeforeEdit.training_date = trainingObj?.training_date;
    this.trainingObjBeforeEdit.start_time = trainingObj?.start_time;
    this.trainingObjBeforeEdit.end_time = trainingObj?.end_time;
    this.trainingObjBeforeEdit.trainer = trainingObj?.trainer;
    this.trainingObjBeforeEdit.topic = trainingObj?.topic;


    this.date = trainingObj?.training_date;
    this.start_time = trainingObj?.start_time;
    this.end_time = trainingObj?.end_time;
    this.trainerForm.setValue({website: trainingObj?.trainer.trainer_id});
    this.topic = trainingObj?.topic;

    console.log(trainingObj);


    this.isEdit = true;
    this.buttonTitle = "Änderungen speichern";
    //this.saveEditedPost(trainingId ? trainingId.toString() : "", trainingObj ? trainingObj : {});
  }

  getPosts(): void {
    this.postTrainingService.list()
      .subscribe(
        data => {
          this.training = data;
          console.log(this.training);
        },
        error => {
          console.error(error);
        });
  }

  getTrainer(): void {
    this.postTrainerService.list()
      .subscribe(
        data => {
          this.trainer = data;
          console.log(this.trainer);
        },
        error => {
          console.error(error);
        });
  }

  getTrainerByID(id: string) {
    return this.postTrainerService.get(id);
  }

  // ?
  deleteEvent(id: string): void {  
    this.postTrainingService.delete(id)
      .subscribe(
        response => {
          console.log(id);
          this.refreshList();
        },
        error => {
          console.error(error);
        });
      }
  
  private saveEditedPost(trainingID?: string, trainingObj?: Training) {
    this.postTrainingService.update(trainingID ? trainingID : "", trainingObj ? trainingObj : {})
      .subscribe(
        response => {
          //this.router.navigate([ '/admin' ]);
          console.log(trainingID); console.log(trainingObj)
          this.refreshList();
        },
        error => {
          console.error(error);
        });
  }


  private createCalendarEvent() {
    this.postTrainingService.create(this.trainingObj)
      .subscribe(
        response => {
          this.router.navigate([ '/admin' ]);
          this.refreshList();
        },
        error => {
          console.error(error);
        });
  }


  refreshList(): void {
    this.getPosts();
  }

}
