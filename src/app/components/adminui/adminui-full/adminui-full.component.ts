import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-adminui-full',
  templateUrl: './adminui-full.component.html',
  styleUrls: ['./adminui-full.component.css']
})
export class AdminuiFullComponent implements OnInit {

  student: string | null = localStorage.getItem("student") ? localStorage.getItem("student") : "0";
  admin: string | null = localStorage.getItem("admin") ? localStorage.getItem("admin") : "0";
  constructor(private router: Router) { }

  ngOnInit(): void {
     if(this.admin == "0" || this.student == "1" || (this.admin == "0" && this.student == "0")){
      this.router.navigate(['/home']).then(() => {
        window.location.reload();
        });
    }
  }

}
