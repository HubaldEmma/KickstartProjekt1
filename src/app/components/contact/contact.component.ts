import { Component, Input, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Trainer } from 'src/app/model/Trainer';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

 trainerName?: string = "";
 email: string = "";
 jobTitle: string = "";
 linkedInLink: string = "";
 profilePic: string = "";
 telephone: number = 0;
 topics: string = "";


  constructor(
    private location: Location,private router: ActivatedRoute) { }

  ngOnInit(): void {
    this.router.params.subscribe(params => {
      this.trainerName = (params['trainerName']);
      this.email = (params['email']);   
      this.jobTitle = (params['jobTitle']);
      this.linkedInLink = (params['linkedInLink']);
      this.profilePic = (params['profilePic']) ? (params['profilePic']) : "https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_960_720.png";
      this.telephone = (params['telephone']);
      this.topics = (params['topics']);
    });

  }
  
  goBack(): void {
    this.location.back();
  }

}
