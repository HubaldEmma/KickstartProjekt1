import { Component, OnInit, SimpleChanges } from '@angular/core';
import { endOfDay, startOfDay } from 'date-fns';
import { CalendarView , CalendarEvent} from 'angular-calendar';
import { PostTrainerService } from 'src/app/services/trainer/post-trainer.service';
import { Training } from 'src/app/model/Training';
import { Router } from '@angular/router';
import { EventColor } from 'calendar-utils';
import { PostTrainingService } from 'src/app/services/training/post-training.service';

import { Trainer } from 'src/app/model/Trainer';



@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {
  viewDate: Date = new Date();
  view: CalendarView = CalendarView.Month;
  CalendarView = CalendarView;
  training?: Training[];
  trainer?: Trainer;
  calendarEvents: CalendarEvent[] = [];
  eventObj?: any;
  isLoad?: boolean; 
  student?: string | null;
  admin?: string | null;

  colorsMain: EventColor = {
    primary: "white",
    secondary: "#480075",
  };
  colorsTrainer: EventColor = {
    primary: "white",
    secondary: "#A100FF"
  } 
   
  constructor(private postTrainingService: PostTrainingService , private postTrainerService: PostTrainerService, private router: Router) { }

  ngOnInit(): void {
    this.student = localStorage.getItem("student") ? localStorage.getItem("student") : "0";
    this.admin = localStorage.getItem("admin") ? localStorage.getItem("admin") : "0";
    if(this.admin  == "0" && this.student == "0"){
      this.router.navigate([""]);
    }else{
      this.getPosts();
    }
 
  }

  ngOnChanges(changes:SimpleChanges){
    this.getPosts();
  }
  setView(view: CalendarView) {
    this.view = view;
  }


  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {    
    console.log(events);
  }

  eventClicked({ event, sourceEvent }: { event: CalendarEvent; sourceEvent:any }): void{
    console.log(event);
    if(event.meta.trainer_id != '35'){
      this.router.navigate(['/contact/', { 
        trainerName: event.meta.first_name  + " "+ event.meta.last_name, 
        email: event.meta.email,
        jobTitle: event.meta.job_title,
        linkedInLink: event.meta.linkedin_link,
        profilePic: event.meta.profile_pic,
        telephone: event.meta.telephone,
        topics: event.meta.topics,
        
      }]);
    }else {
      alert("Kein Trainer!");
    }
   
   
  }

  setViewDate(date: any){
    this.viewDate = date;
  }
  
  compareDate (emp1: any, emp2: any) {  
    var emp1Date = new Date(emp1.date).getTime();  
    var emp2Date = new Date(emp2.date).getTime();  
    return emp1Date > emp2Date ? 1 : -1;    
}  

  dateFormat(start: Date, end: Date){
    return end != null ? new Date(end.getTime() - start.getTime()).getMinutes(): start.getHours();
  }

  async formatTraining(training: Training[]) {
    
    if(training != null){
      for(let post of training){
        this.eventObj = {start: new Date(), end: new Date(),title: "", meta: ""};
        this.eventObj.id = post.trainingID;
        this.eventObj.title = post.topic;
        let start = new Date(post.training_date + "T" + post.start_time);
        let end = new Date(post.training_date + "T" + post.end_time);
        this.eventObj.start = start;
        this.eventObj.end = end;
        this.eventObj.meta = post.trainer;
        if(post.trainer.trainer_id != '35') {
          this.eventObj.color = this.colorsTrainer;
        }else {
          this.eventObj.color = this.colorsMain;
          console.log(this.eventObj.meta);
        }
        this.calendarEvents.push(this.eventObj);
      }
    }

  console.log(this.calendarEvents);  
  }


  
  getPosts(): void {
    this.postTrainingService.list()
      .subscribe(
        data => {
          //this.training = data;
          //console.log(this.training);
          console.log(data);
          this.formatTraining(data);
          this.setView(CalendarView.Month);
          this.isLoad = true;
        },
        error => {
          console.error(error);
        });
  }

    getTrainerByID(id: string) {
    return this.postTrainerService.get(id);
  }
}
